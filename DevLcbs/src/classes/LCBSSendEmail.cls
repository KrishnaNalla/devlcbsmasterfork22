/*
Class Name : LCBSSendEmail
Functionality: This is a reusable class to send Email Messages
Created by: Nagarajan Varadarajan
*/
  
  
public class LCBSSendEmail{
public static List<Messaging.SingleEmailMessage> mails {get;set;}
public void LCBSSendEmail()
{

}
public static void Email(List<String> sendTo,String subject,String DisplayName,String Message)
{
    mails = new List<Messaging.SingleEmailMessage>();
    Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage(); 
    mail.setToAddresses(sendTo);
    mail.setSubject(subject);
    mail.setSenderDisplayName(DisplayName);
    mail.setHTMLBody(Message);
    
    mails.add(mail);
    system.debug('mails ########:'+mails);
    if(mails.size()>0){
    Messaging.sendEmail(mails);
    }
}
}
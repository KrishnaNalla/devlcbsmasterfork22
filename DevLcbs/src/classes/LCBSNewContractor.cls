public without sharing class LCBSNewContractor
{
// public Account a {get;set;}
 Public List<Account> AccountList {get;set;}
 public List<Contact> contact {get;set;}
 public Contact con {get;set;}
 public String CPRONumber {get;set;}
 public String Email {get;set;}
// public List<Partner> part {get;set;}
 public Partner p {get;set;}
 public List<Account> a {get;set;}
 public List<Messaging.SingleEmailMessage> mails {get;set;}
 public List<Messaging.SingleEmailMessage> sitemails {get;set;}
 public LCBSNewContractor()
 {
  
  AccountList = new List<Account>();
    contact = new List<Contact>();
  con = new Contact();
  //part = new List<Partner>();
 }
 public PageReference createContractor()
 {
    RecordType RT = [select id,Name from RecordType where Name = 'ECC:Contractor Contacts'];
    con.RecordTypeId = RT.id;
    con.LastName='Test'; //added for test class
    insert con;   
    Task task = new Task();
    task.WhatId = con.AccountId;
    task.WhoId = con.id;
    task.Subject = 'New Contractor Request-Email Sent';
    task.status = 'Completed';
    task.Description ='Created By:   ' + '\r\n' + userinfo.getName() +'\r\n'+'Created Time:   '+system.now() + '\r\n';
    task.ActivityDate = Date.today();
    insert task;     
    PageReference p = new PageReference('/LCBS_Contractor_page2');
    p.setRedirect(true);
    return p;
 }
 
  public SelectOption[] getCompanyLoc() 
    { 
        try
        {      
            SelectOption[] getCompanyLocNames= new SelectOption[]{};  
                getCompanyLocNames.add(new SelectOption('','--None--'));
                RecordType RT = [select id,Name from RecordType where Name = 'Contractors'];
            for (Account acc : [select id,Name from Account where recordtypeid=:RT.Id order by name Limit 999]) 
            {  
                getCompanyLocNames.add(new SelectOption(acc.id,acc.Name));   
                
            }
            return getCompanyLocNames; 
        }   
        catch(NullPointerException npe)
        {
            ApexPages.addMessages(npe);    
        }
        return null;
    }  
  public PageReference EmailContractor()
  {
        try{
            List<String> sendTo = new List<String>();
           
            sendTo.add(Email);
            mails= new List<Messaging.SingleEmailMessage>();          
            Messaging.SingleEmailMessage sitemail = new Messaging.SingleEmailMessage();
            sitemail.setToAddresses(sendTo);
            sitemail.setSenderDisplayName('New Contractor Request');
            sitemail.setSubject('Register here');            
          // string emailMessage ='Please follow the link to register yourself as Contractor <br/> <br/> https://devlcbs-totalbuildings.cs17.force.com/CommunitiesSelfReg';
           //  string emailMessage ='Please follow the link to register yourself as Contractor <br/> <br/> <a hrefhttp://devlcbs-devlcbs-iframe.cs17.force.com/EULAContractor';
             string emailMessage ='<a href="http://devlcbs-devlcbs-iframe.cs17.force.com/EULAContractor">Please follow the link to register yourself as Contractor</a>';
            sitemail.setHtmlBody(emailMessage);
            mails.add(sitemail);
            Messaging.sendEmail(mails); 
            
            }
            
            catch (Exception ex)
            {
            system.debug(ex.getMessage());
            
            }  
            PageReference p = new PageReference('/apex/LCBS_New_Contractor');
            p.setRedirect(true); 
            return p; 
            
            
            
  }
  public PageReference addCPROContractor()
  {
     User u = [Select id,Contact.AccountId,Account.email__c from User where id=:UserInfo.getUserId()];
     // Contact c = [Select id,Name,CPRO_Number__c from Contact where CPRO_Number__c=:CPRONumber];
     List<Account> acc = [Select Id , Name, EnvironmentalContractorProNumber__c from Account where EnvironmentalContractorProNumber__c=:CPRONumber  OR Name=:CPRONumber limit 1];
     if(acc.size() > 0)
     {  
         System.debug('Naga Test Account Result:'+acc);
         System.debug('Naga Test CPRO Number:'+CPRONumber);
         a = new List<Account>();
         a = [select id,Name from Account where ID IN (Select AccountToId from Partner where AccountToId =:acc[0].id) limit 1];     
         if(a.size() > 0)
         {
            ApexPages.Message Msg = new ApexPages.Message(ApexPages.Severity.ERROR, 'The Contractor is already assigned to another Distributor. Please check your Email for more details.');
            ApexPages.addMessage(Msg); 
            List<String> sendTo1 = new List<String>();            
            sitemails = new List<Messaging.SingleEmailMessage>();          
            Messaging.SingleEmailMessage sitemail = new Messaging.SingleEmailMessage();
            system.debug('ChiruTest*****:'+u.id);
            system.debug('ChiruTest#####:'+u.AccountId);
            sendTo1.add(u.Account.email__c);
            sitemail.setToAddresses(sendTo1);
            sitemail.setSenderDisplayName('Contractor Details');
            sitemail.setSubject('Contractor is already Assigned');            
            string emailMessage ='Contractor Name : '+ ' '+acc[0].Name+' is Assigned to '+' '+a[0].Name;
            sitemail.setHtmlBody(emailMessage);
            sitemails.add(sitemail);
            Messaging.sendEmail(sitemails);   
              
                     
         }
         else {
             p = new Partner();
             p.AccountFromId= acc[0].id;
             p.AccountToId= u.Contact.AccountId;
          // p.AccountToId= '001g000000GW8hD';
             insert p;
             System.debug('Partner Record inserted');
             PageReference pa = new PageReference('/apex/LCBSContractorPage');
             pa.setRedirect(true);
             // return pa;  
            } 
         }
     else {
     system.debug('Naga - Test: Invalid CPRO Number');
     ApexPages.Message myMsg = new ApexPages.Message(ApexPages.Severity.ERROR, 'Invalid CPRO Number');
     ApexPages.addMessage(myMsg);
     List<String> sendTo2 = new List<String>();
            sitemails = new List<Messaging.SingleEmailMessage>();          
            Messaging.SingleEmailMessage sitemail = new Messaging.SingleEmailMessage();
            sendTo2.add(u.Account.email__c);
            sitemail.setToAddresses(sendTo2);
            sitemail.setSenderDisplayName('Contractor Details');
            sitemail.setSubject('Invalid Contractor details');            
            string emailMessage ='Invalid Contractor Details. Please follow the link to register for a New CPRO Number. <br/> www.contractorpro.com';
            sitemail.setHtmlBody(emailMessage);
            sitemails.add(sitemail);
            if(!Test.isRunningTest()){ 
            Messaging.sendEmail(sitemails); 
            }
     }
     
    
   PageReference pa = new PageReference('/LCBS_New_Contractor');
             pa.setRedirect(true);
          return pa;  
           // return null;
  }  
    
}
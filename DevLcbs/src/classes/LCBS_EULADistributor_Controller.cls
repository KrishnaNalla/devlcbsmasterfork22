/************************************
Name         : LCBS_EULADistributor_Controller
Created By   : Nagarajan Varadarajan
Company Name : NTT Data
Project      : LCBS
Created Date : 
Test Class   : 
Test Class URL: 
Usages       : 
Modified By  :
***********************************/

public class LCBS_EULADistributor_Controller{

    public Account fetchaccinfo{get; set;}
    public Contact fetchconinfo{get; set;}
    public String currentRecordId{get; set;}
    // public String errorLevel{get; set;}
    public String messageName{get; set;}
    public boolean displayPopup{get; set;}
    public string executive_name{get; set;}
    public string executive_email{get; set;}
    public string executive_phone{get; set;}
    public Account c{get; set;}
    
    public void closePopup(){
        displayPopup = false;
    }
    
    public void showPopup(){
        displayPopup = true;
    }
    
    public void showMessage(){
        ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, messageName));
    }
    
    public boolean Approved{
        get{
            if(fetchaccinfo.EULA_Accepted__c== true){
                return true;
            }
            else{
                return false;
            }
        }
        set;
    }
    
    public boolean rejected{
        get{
            if(fetchaccinfo.EULA_Rejected__c== true){
                return true;
            }
            else{
                return false;
            }
        }
        set;
    }
    
    public LCBS_EULADistributor_Controller(){
        fetchaccinfo = new Account();
        currentRecordId  = ApexPages.CurrentPage().getparameters().get('id');
        fetchaccinfo = [select id, Name, email__c, Account_Exec_Name__c, Account_Exec_Email__c, Account_Exec_Phone__c, Date_EULA_Accepted__c, EULA_Accepted__c, EULA_Rejected__c from account where id =:currentRecordId];
        // Account_Executive_Info__c
        fetchconinfo = [Select id, Contractor_EULA_Accepted__c, Contractor_Eula_Rejected__c, Date_EULA_Sent__c, LCBS_UserName__c, LCBS_Password__c, LCBS_FedId__c, FirstName, LastName, Email, phone, AccountId, LCBS_Program_Principal__c from Contact where AccountId =:fetchaccinfo.id and LCBS_Program_Principal__c = true];
        system.debug('*****fetchinfo '+fetchaccinfo);
        // c = [select id,name,(select id,firstname,lastname,email,phone from contacts) from Account where id=:system.label.Honeywell_Account_Executive];
        // system.debug('%%'+c.contacts[0].firstname);
        
        if(fetchaccinfo.Account_Exec_Name__c != NULL){
            executive_name = fetchaccinfo.Account_Exec_Name__c;
        }
        else{
            executive_name = '';
        }
        
        if(fetchaccinfo.Account_Exec_Email__c != NULL){
            executive_email = fetchaccinfo.Account_Exec_Email__c;
        }
        else{
            executive_email = '';
        }
        
        if(fetchaccinfo.Account_Exec_Phone__c != NULL){
            executive_phone = fetchaccinfo.Account_Exec_Phone__c;
        }
        else{
            executive_phone = '';
        }
        
        /* if(fetchaccinfo.Account_Executive_Info__c != null){
            // String[] exec = fetchaccinfo.Account_Executive_Info__c.split('\\|');
            executive_name = fetchaccinfo.Account_Exec_Name__c;
            executive_email = fetchaccinfo.Account_Exec_Email__c;
            executive_phone = fetchaccinfo.Account_Exec_Phone__c;
        }
        else
        {
            executive_name = '';
            executive_email = '';
            executive_phone = '';
        } */
        
    }
   
     public PageReference approveAction()
        {
          string url = system.label.LCBSUserResponseURL+'?id='+fetchconinfo.id;
           try
            {
            system.debug('*****fetchinfo '+fetchaccinfo); 
            LCBSAddUserCallout.onAfterEulaAccept(fetchconinfo.id);
            fetchaccinfo.EULA_Accepted__c=True;
            fetchaccinfo.Date_EULA_Accepted__c=system.today();
            update fetchaccinfo;
           // Approved = true;
            
            
            }
            catch (DMLException ex){
            system.debug(ex.getMessage());
            ApexPages.Message msg = new ApexPages.Message(ApexPages.Severity.ERROR,'Account does not exists');
            ApexPages.addMessage(msg);
            
            Approved = false;
             }
          PageReference pr = new PageReference(url);
          return pr;
         /*PageReference p = new PageReference('/LCBSDistributorAgreement?Id='+currentRecordId);
         p.setRedirect(true); 
         return p;*/
        }
        
      public void rejectAction() {      
        try
            {
             displayPopup = false;
            system.debug('*****fetchinfo***** '+fetchaccinfo); 
            fetchaccinfo.EULA_Rejected__c=True;
            fetchaccinfo.Distributor_Invited__c = false;
            update fetchaccinfo;
        /*  List<Messaging.SingleEmailMessage> sitemails = new List<Messaging.SingleEmailMessage>();          
            Messaging.SingleEmailMessage sitemail = new Messaging.SingleEmailMessage();
            Account acc = [Select id, Email__c from Account where id = '001g000000Zii5c'];
            Account acc1 = [Select id, Email__c from Account where id = '001g000000Zii5S'];
            List<String> sendTo = new List<String>();
            sendTo.add(acc.Email__c);
            sendTo.add(acc1.Email__c);
            sitemail.setToAddresses(sendTo);
            sitemail.setSenderDisplayName('LCBS Portal');
            sitemail.setSubject('Distributor Rejected');            
            string emailMessage ='Distributor Name : '+fetchaccinfo.Name;
            sitemail.setHtmlBody(emailMessage);
            sitemails.add(sitemail);
            Messaging.sendEmail(sitemails);  */  
            Rejected = true;   
            }
            catch (DMLException ex){
            system.debug(ex.getMessage());
            ApexPages.Message msg = new ApexPages.Message(ApexPages.Severity.ERROR,'Account does not exists');
            ApexPages.addMessage(msg);
            
            Rejected= false;
             } 
        /*  PageReference p1 = new PageReference('/LCBSDistributorAgreement?Id='+currentRecordId);
          p1.setRedirect(true); 
          return p1;*/
    }

}
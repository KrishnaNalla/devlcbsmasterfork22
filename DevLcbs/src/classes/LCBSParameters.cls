public class LCBSParameters {
    String Env;
    String AzurePageCSSUrl;
    String AzureHeaderCSSUrl;
    String AzureBuildingsUrl;
    String AzureSignOutUrl;
    String InvContractorSendUsMailIds;
    String AzureDomain;
    String AzureHome;
    //Env: returns the current environment of the application - DEVLCBS/TSTLCBS/SIT/PROD
    public Static String Env {
    
        get {       
        if (!Test.isRunningTest()) { 
            
            String CommunityId = Network.getNetworkId();
            System.debug('CommunityId:::'+CommunityId);            
            String CommunityUrl = Network.getLoginUrl(CommunityId);
            System.debug('CommunityUrl:::'+CommunityUrl);
           Env = CommunityUrl.remove('https://').substringBefore('-');
          // Env = 'devlcbs';
            System.debug('Environment:::'+Env);
            
          }
         if (Test.isRunningTest()) {
            String CommunityId ='0DBg000000000UQ' ;
            String CommunityUrl ='https://devlcbs-totalbuildings.cs17.force.com/';
            Env = CommunityUrl.remove('https://').substringBefore('-');                           
            }  
           return Env;
            
        }
    }
    
    //AzurePageCSSUrl: returns the PAGE CSS URL from Azure CDN
    public Static String AzurePageCSSUrl {
    
        get {
        if (!Test.isRunningTest()) {        
            AzurePageCSSUrl = LCBS_AzureUrls__c.getvalues(Env).LCBS_PageCSSUrl__c;
            }
        if (Test.isRunningTest()) {
            String Env1 = 'sit';
        
            System.debug('$$$$$$$$$:'+LCBS_AzureUrls__c.getvalues(Env1).LCBS_PageCSSUrl__c);
            AzurePageCSSUrl = LCBS_AzureUrls__c.getvalues(Env1).LCBS_PageCSSUrl__c;
            }
            return AzurePageCSSUrl;      
        }
    }
    
    public Static String AzureDomain{
    
        get {
        if (!Test.isRunningTest()) {        
            AzureDomain= LCBS_AzureUrls__c.getvalues(Env).Tenant_Domain__c;
            }
        if (Test.isRunningTest()) {
            String Env1 = 'devlcbs';
        
            System.debug('$$$$$$$$$:'+LCBS_AzureUrls__c.getvalues(Env1).Tenant_Domain__c);
            AzureDomain= LCBS_AzureUrls__c.getvalues(Env1).Tenant_Domain__c;
            }
            return AzureDomain;      
        }
    }
    
     public Static String AzureHome{
    
        get {
        if (!Test.isRunningTest()) {        
            AzureHome= LCBS_AzureUrls__c.getvalues(Env).Tenant_HomePage__c;
            }
        if (Test.isRunningTest()) {
            String Env1 = 'devlcbs';
        
            System.debug('$$$$$$$$$:'+LCBS_AzureUrls__c.getvalues(Env1).Tenant_HomePage__c);
            AzureHome= LCBS_AzureUrls__c.getvalues(Env1).Tenant_HomePage__c;
            }
            return AzureHome;      
        }
    }
    
    
    //AzureHeaderCSSUrl: returns the HEADER CSS from Azure CDN
    public Static String AzureHeaderCSSUrl {
        
        get {
        if (!Test.isRunningTest()) { 
            AzureHeaderCSSUrl =  LCBS_AzureUrls__c.getvalues(Env).LCBS_HeaderCSSUrl__c;
            }
        if (Test.isRunningTest()) { 
            String Env2 = 'devlcbs';
            AzureHeaderCSSUrl =LCBS_AzureUrls__c.getvalues(Env2).LCBS_HeaderCSSUrl__c;
            }
            return AzureHeaderCSSUrl;
        }
    }
    
    //AzureBuildingsUrl: returns the Azure All Buildings URL
    public Static String AzureBuildingsUrl {
        
        get {
        if (!Test.isRunningTest()) { 
            AzureBuildingsUrl =  LCBS_AzureUrls__c.getvalues(Env).LCBS_HomeUrl__c;
            }
        if (Test.isRunningTest()) {
            String Env3 ='devlcbs';       
            AzureBuildingsUrl =LCBS_AzureUrls__c.getvalues(Env3).LCBS_HomeUrl__c;
            }
            return AzureBuildingsUrl;
        }
    }
    
    //AzureSignOutUrl: returns the Azure Signout Url
    public Static String AzureSignOutUrl {
        
        get {
        if (!Test.isRunningTest()) { 
            AzureSignOutUrl =  LCBS_AzureUrls__c.getvalues(Env).LCBS_SignOutUrl__c;
            }
        if (Test.isRunningTest()) { 
            String Env4 = 'devlcbs';
            AzureSignOutUrl =  LCBS_AzureUrls__c.getvalues(Env4).LCBS_SignOutUrl__c;
            }
            return AzureSignOutUrl;
        }
    }
    
    //InvContractorSendMailIds: returns the email ids for Invite Contractor Send Us an Email notification
    public Static List<String> InvContractorSendUsMailIds {
        
        get {
        if (!Test.isRunningTest()) { 
            InvContractorSendUsMailIds =  LCBS_NotificationMailIds__c.getvalues(Env).LCBS_InvContractorSendMailIds__c.split('\\,');
            }
        if (!Test.isRunningTest()) { 
            String Env5 = 'devlcbs';
            InvContractorSendUsMailIds =  LCBS_NotificationMailIds__c.getvalues(Env5).LCBS_InvContractorSendMailIds__c.split('\\,');
            }
            return InvContractorSendUsMailIds;
        }
    }
    
    public Static String CURRENT_ENV = Env;
    public Static String PAGE_CSS_URL = AzurePageCSSUrl;
    public Static String HEADER_CSS_URL = AzureHeaderCSSUrl;
    public Static String DISTRIBUTOR_HOME_URL = AzureBuildingsUrl;
    public Static String AZURE_SIGNOUT_URL = AzureSignOutUrl;
    public Static String HomePage_URL = AzureHome;
    public Static String Domain_URL= AzureDomain;
    public Static List<String> INV_CONTRACTOR_SND_US_MAIL_IDS = InvContractorSendUsMailIds;
}
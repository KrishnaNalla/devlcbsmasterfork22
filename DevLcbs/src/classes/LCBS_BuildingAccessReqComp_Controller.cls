public without sharing class LCBS_BuildingAccessReqComp_Controller{
    public string BuildOwnerId{get;set;}
public string BuildOwnerName{get;set;}
   
public string BuildId{get;set;}    

  public String BuldId {get;set;}                    
public string orgid {get;set;}
public document logo {get;set;}
public document sign {get;set;}
public string sfdcBaseURL{get;set;}
public String companyId {get;set;}

public Id buildingOwnerId{get;set;}
public string buildingOwnerName{get;set;}
public Contact con {get;set;}
public String siteurl {get;set;}
public Building_Owner__c Bo{get;set;}
public user u{get;set;}
public Building__c build {get;set;}
  public Building__c fetchinfo {
        get {
            build = [select id,createdBy.id from Building__c where id =: BuldId];
            system.debug('fetchinfo<<<<<<<<:'+fetchinfo);
            return fetchinfo;                        
        } 
        set;
    }   
    
 public LCBS_BuildingAccessReqComp_Controller()
 {
   BuldId = ApexPages.CurrentPage().getparameters().get('id');
  // build = [select id,createdBy.id,Building_Owner__r.id from Building__c where id=:BuldId];
   system.debug(build);
 // u = [select id,name,AccountId from user where id=:build.createdBy.id];
  sfdcBaseURL = System.URL.getSalesforceBaseURL().toExternalForm();
  // con = [select id,name,phone,Account.name,Account.Email__c from Contact where AccountId=:u.AccountId and Role__c='Owner' limit 1];
   system.debug(con);
    Site ste = [SELECT Id, Name, Description FROM Site WHERE Name='LCBS_BuildingApproveReject'];
     siteurl = ste.description;
     system.debug('site URL>>>>>>:'+siteurl);
  sign = [select id,name from document where name='Honeywell vicepresident sign']; 
  orgid= UserInfo.getOrganizationId();
  logo = [select id,name from document where name='LCBS_Email_Logo']; 
  
 }
}
trigger LCBSDisplayDistributorContact on Contact (after insert,after update,before insert,before update) {
 
  for(Contact c: Trigger.New)
  {
   if(c.LCBS_Program_Principal__c == true && c.AccountId != NULL)
    {
     Account a =[select id,name,Distributor_Principle__c from Account where id=:c.AccountId];
     a.Distributor_Principle__c = c.id;
     update a;
    }
  
 }
}